package mips;

/**
 * Class Main (initiate the program).
 *
 * @author JohnVithor.
 */
public class Main {

    /**
     * main method (initiate the program).
     *
     * @param args
     *            arguments of the program (actually don't receive any).
     */
    public static void main(final String[] args) {
        Boolean boolPath = false;
        String path = "";
        for (String arg : args) {
            if (arg.contains("PATH:")) {
                final String[] argPath = arg.split(":");
                path = argPath[1];
                boolPath = true;
            } else {
                System.out.println("O argumento '" + arg + "' não existe, "
                        + "verifique se digitou corretamente ou se este argumento existe");
                return;
            }
        }
        if (!boolPath) {
            System.out.println("Não foi informado o caminho do arquivo com as instruções.\n"
                    + "Por favor informe um caminho para o arquivo com as instruções.\n"
                    + "Utilize o parametro 'PATH:' e em seguida o caminho a ser utilizado.");
            return;
        }
        final String instructionPath = path;
        final ProcessorMips mips = new ProcessorMips(instructionPath);
        System.out.println("Utilizando as instruções do arquivo: " + path);
        System.out.println("------------------------");
        do {
            mips.process();
            System.out.println("Ciclo " + mips.getCycles());
            System.out.println(mips);
            System.out.println("------------------------");
        } while (!mips.hasTerminate());
        System.out.println("Esse conjunto de instruções levou " + mips.getCycles()
                + " ciclos para ser executado.\n");
        return;
    }
}