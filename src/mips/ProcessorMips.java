package mips;

/**
 * Represents an Processor based in the MIPS processor, but with fewer instructions.
 * 
 * @author JohnVithor.
 *
 */
public class ProcessorMips {
    private Integer pc;
    private Integer cycles;
    private Memory instructions;

    private enum PipelineStages {
        FIND, TODECODE, DECODE, TOEXECUTE, EXECUTE, TOMEMORY, MEMORY, TOWRITE, WRITE;
    }

    private Instruction[] pipelineStages;

    /**
     * Default Constructor
     * 
     * @param instructionsPath
     */
    public ProcessorMips(final String instructionsPath) {
        this.pipelineStages = new Instruction[10];
        this.pc = 0;
        this.cycles = 0;
        this.instructions = new Memory(instructionsPath);
    }

    /**
     * Checks if the loaded program has terminated.
     * 
     * @return true if the loaded program has terminated, false otherwise.
     */
    public final Boolean hasTerminate() {
        Instruction instruction = null;
        for (Integer i = PipelineStages.FIND.ordinal(); i <= PipelineStages.TOWRITE
                .ordinal(); ++i) {
            instruction = pipelineStages[i];
            if (instruction != null) {
                return false;
            }
        }
        return true;
    }

    /**
     * 
     * @param instructionsPath
     *            path to the instruction memory.
     */
    public final void newIntructions(final String instructionsPath) {
        this.instructions = new Memory(instructionsPath);
        this.pc = 0;
    }

    /**
     * process the instructions using a pipeline.
     */
    public final void process() {
        find();
        decode();
        execute();
        memory();
        write();
        movePipeline();
        ++cycles;
    }

    private final void movePipeline() {
        pipelineStages[PipelineStages.TOWRITE.ordinal()] = pipelineStages[PipelineStages.MEMORY
                .ordinal()];
        pipelineStages[PipelineStages.TOMEMORY.ordinal()] = pipelineStages[PipelineStages.EXECUTE
                .ordinal()];
        pipelineStages[PipelineStages.TOEXECUTE.ordinal()] = pipelineStages[PipelineStages.DECODE
                .ordinal()];
        pipelineStages[PipelineStages.TODECODE.ordinal()] = pipelineStages[PipelineStages.FIND
                .ordinal()];
    }

    private void find() {
        Instruction i = new Instruction(instructions.getData(pc));
        if (instructions.getData(pc).equals("end")) {
            pipelineStages[PipelineStages.FIND.ordinal()] = null;
        }
        InstructionType type = InstructionType.getType(i.getOpcode());
        switch (type) {
            case ATYPE:
                InstructionArith aType = new InstructionArith(i.getInstruction());
                if (hasConflict(aType)) {
                    pipelineStages[PipelineStages.FIND.ordinal()] = null;
                    return;
                }
                pipelineStages[PipelineStages.FIND.ordinal()] = aType;
                break;
            case BTYPE:
                InstructionBranch bType = new InstructionBranch(i.getInstruction());
                if (hasConflict(bType)) {
                    pipelineStages[PipelineStages.FIND.ordinal()] = null;
                    return;
                }
                pc = Integer.parseInt(bType.getLabel()) - 1;
                pipelineStages[PipelineStages.FIND.ordinal()] = bType;
                return;
            case JTYPE:
                InstructionJump jType = new InstructionJump(i.getInstruction());
                pc = Integer.parseInt(jType.getLabel()) - 1;
                pipelineStages[PipelineStages.FIND.ordinal()] = jType;
                return;
            case MTYPE:
                InstructionMemory mType = new InstructionMemory(i.getInstruction());
                if (hasConflict(mType)) {
                    pipelineStages[PipelineStages.FIND.ordinal()] = null;
                    return;
                }
                pipelineStages[PipelineStages.FIND.ordinal()] = mType;
                break;
            case UNKNOW:
                break;
        }
        ++pc;
    }

    private Boolean hasConflict(Instruction3 testing) {
        String needed = null;
        Instruction instruction = null;
        for (Integer i = PipelineStages.TODECODE.ordinal(); i < PipelineStages.TOMEMORY
                .ordinal(); ++i) {
            instruction = pipelineStages[i];
            if (instruction == null) {
                continue;
            }
            if (instruction.getOpcode() == Opcodes.LW || instruction instanceof InstructionArith) {
                needed = ((Instruction2) instruction).get1();
                if (testing.getOperand1().equals(needed) || testing.getOperand2().equals(needed)) {
                    return true;
                }
            }
        }
        return false;
    }

    private Boolean hasConflict(InstructionMemory testing) {
        String needed = null;
        Instruction instruction = null;
        for (Integer i = PipelineStages.TODECODE.ordinal(); i < PipelineStages.TOMEMORY
                .ordinal(); ++i) {
            instruction = pipelineStages[i];
            if (instruction == null) {
                continue;
            }
            if (instruction.getOpcode() == Opcodes.LW || instruction instanceof InstructionArith) {
                needed = ((Instruction2) instruction).get1();
                if (testing.getMemoReg().equals(needed)) {
                    return true;
                }
            }
            if (testing.getOpcode() == Opcodes.SW) {
                if (testing.getReg().equals(needed)) {
                    return true;
                }
            }
            if (instruction.getOpcode() == Opcodes.SW && testing.getOpcode() == Opcodes.LW) {
                needed = ((InstructionMemory) instruction).getMemoReg();
                if (testing.getMemoReg().equals(needed)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void decode() {
        Instruction i = pipelineStages[PipelineStages.TODECODE.ordinal()];
        pipelineStages[PipelineStages.DECODE.ordinal()] = i;

    }

    private void execute() {
        Instruction i = pipelineStages[PipelineStages.TOEXECUTE.ordinal()];
        pipelineStages[PipelineStages.EXECUTE.ordinal()] = i;
    }

    private void memory() {
        Instruction i = pipelineStages[PipelineStages.TOMEMORY.ordinal()];
        pipelineStages[PipelineStages.MEMORY.ordinal()] = i;
    }

    private void write() {
        Instruction i = pipelineStages[PipelineStages.TOWRITE.ordinal()];
        pipelineStages[PipelineStages.WRITE.ordinal()] = i;
    }

    @Override
    public final String toString() {
        return "IF:  " + pipelineStages[PipelineStages.FIND.ordinal()] + "\n" + "ID:  "
                + pipelineStages[PipelineStages.DECODE.ordinal()] + "\n" + "EXE: "
                + pipelineStages[PipelineStages.EXECUTE.ordinal()] + "\n" + "MEM: "
                + pipelineStages[PipelineStages.MEMORY.ordinal()] + "\n" + "WB:  "
                + pipelineStages[PipelineStages.WRITE.ordinal()];
    }

    /**
     * 
     * @return the number of cycles that the processor has utilized.
     */
    public Integer getCycles() {
        return this.cycles;
    }
}
