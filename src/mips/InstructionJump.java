package mips;

/**
 * Represents an instruction in its MIPS assembly format of the jump type.
 *
 * @author JohnVithor.
 */
public class InstructionJump extends Instruction1 {

    /**
     * The default constructor of an instruction, receives a string with the statement in MIPS
     * assembly.
     *
     * @param instruction
     *            String with an instruction.
     */
    public InstructionJump(final String instruction) {
        super(instruction);
    }

    /**
     * Getter for the parameter that indicates where the next instruction is.
     *
     * @return parameter that indicates where the next instruction is.
     */
    public final String getLabel() {
        return get1();
    }

}
