package mips;

/**
 * Represents an instruction that should only have THREE parameter after the OPCODE.
 *
 * @author JohnVithor.
 */
public abstract class Instruction3 extends Instruction2 {

    /**
     * The default constructor of an instruction, receives a string with the statement in MIPS
     * assembly.
     *
     * @param instruction
     *            String with an instruction.
     */
    public Instruction3(final String instruction) {
        super(instruction);
    }

    /**
     * Returns the third parameter after the OPCODE of the instruction.
     *
     * @return the third parameter after the OPCODE of the instruction.
     */
    public final String get3() {
        return getParsed((short) 3).replace(",", "");
    }

    /**
     * Getter for the first operand of the instruction.
     *
     * @return first operand of the instruction.
     */
    public abstract String getOperand1();

    /**
     * Getter for the second operand of the instruction.
     *
     * @return second operand of the instruction.
     */
    public abstract String getOperand2();

}
